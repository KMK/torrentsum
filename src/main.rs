use std::cmp::Ordering;
use std::fmt;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};
use bip_metainfo::iter::{Files, Pieces};
use bip_metainfo::Metainfo;
use bip_metainfo::error::ParseError as BipParseError;
use sha1_smol::Sha1;

const BUFFER_LEN: usize = 4096;

#[derive(Clone, Debug, PartialEq)]
enum Error {
    Io(std::io::ErrorKind),
    /// Torrent file failed to parse
    BipParseError(String),
    /// No more files to check while there are remaining pieces
    UnexpectedEndOfFiles,
    /// No more pieces to check while there are remaining files
    UnexpectedEndOfPieces,
    /// A file is shorter than torrent's claim
    UnexpectedEof,
    /// A file is longer than torrent's claim
    FileTooLong,
    /// A piece failed to match the actual content of a file
    ChecksumFailed,
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Io(err) =>
                write!(f, "input output error: {}", err),
            Error::BipParseError(s) =>
                write!(f, "failed to parse torrent file: {}", s),
            Error::UnexpectedEndOfPieces =>
                write!(f, "unexpected end of pieces"),
            Error::UnexpectedEndOfFiles =>
                write!(f, "unexpected end of files"),
            Error::UnexpectedEof =>
                write!(f, "short file"),
            Error::FileTooLong =>
                write!(f, "file too long"),
            Error::ChecksumFailed =>
                write!(f, "checksum failed"),
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Error::Io(err.kind())
    }
}

impl From<BipParseError> for Error {
    fn from(err: BipParseError) -> Self {
        Error::BipParseError(format!("{}", err))
    }
}

struct Current<'a> {
    file: File,
    file_remaining: u64,
    piece: &'a [u8],
    piece_remaining: u64
}

impl<'a> Current<'a> {
    // Read one "chunk" of data in `buffer` and update the `hasher`
    // with it.
    //
    // A chunk of data is any length of data that is practical to work
    // with. `read_one_chunk()` will try to use the full buffer, but
    // will use smaller length if the current piece or the current
    // file does not have enough remaining bytes left.
    fn read_one_chunk(
        &mut self,
        buffer: &mut [u8],
        info: &mut Info<'a>,
    ) -> Result<(), Error> {
        // Process one chunk
        let chunk_len = buffer.len()
            .min(
                self.file_remaining.min(self.piece_remaining)
                    .try_into()
                    .unwrap_or(usize::MAX)
            );
        let chunk = &mut buffer[..chunk_len];
        self.file.read_exact(chunk)?;
        info.hasher.update(chunk);
        let chunk_len = chunk_len.try_into()
            .expect("Chunk length fits in a u64");
        self.file_remaining = self.file_remaining
            .checked_sub(chunk_len)
            .expect("Can't read more than what was remaining in file");
        self.piece_remaining = self.piece_remaining
            .checked_sub(chunk_len)
            .expect("Can't read more than what was remaining in piece");
        Ok(())
    }
}

struct FilesChecked<'a> {
    base: PathBuf,
    files: Files<'a>
}

impl<'a> FilesChecked<'a> {
    fn new(base: PathBuf, files: Files<'a>) -> Self {
        Self { base, files }
    }
}

impl<'a> Iterator for FilesChecked<'a> {
    type Item = Result<(File, u64), Error>;
    fn next(&mut self) -> Option<Self::Item> {
        let bip_file = self.files.next()?;
        let length = bip_file.length();
        Some(
            File::open(self.base.join(bip_file.path()))
                .map_err(Error::from)
                .and_then(
                    |f| match f.metadata()?.len().cmp(&length) {
                        Ordering::Less => Err(Error::UnexpectedEof),
                        Ordering::Greater => Err(Error::FileTooLong),
                        Ordering::Equal => Ok((f, length))
                    }
                )
        )
    }
}

enum Outcome<'a> {
    InProgress(Current<'a>),
    Ok,
    Err(Error)
}

impl<'a> Outcome<'a> {
    fn new(
        pieces: &mut Pieces<'a>,
        files: &mut FilesChecked<'a>,
        piece_length: u64,
    ) -> Self {
        match (pieces.next(), files.next()) {
            // Move onto the next chunk of work
            (Some(piece), Some(Ok((file, file_remaining)))) =>
                Outcome::InProgress(Current {
                    file,
                    file_remaining,
                    piece,
                    piece_remaining: piece_length,
                }),
            (Some(_), Some(Err(err))) => Outcome::Err(err.into()),
            // Nothing to do: no pieces and no files
            (None, None) => Outcome::Ok,
            // FIXME This is *not idempotent: a piece has been consumed
            (Some(_), None) =>
                Outcome::Err(Error::UnexpectedEndOfFiles),
            // FIXME This is *not idempotent: a file has been consumed
            (None, Some(_)) =>
                Outcome::Err(Error::UnexpectedEndOfPieces),
        }
    }
}

struct Info<'a> {
    piece_length: u64,
    pieces: Pieces<'a>,
    files: FilesChecked<'a>,
    hasher: Sha1,
}

impl<'a> Info<'a> {
    // Returns `Ok(true)` if there is more work to do. Returns
    // `Ok(false)` if there is nothing more to do, that is all the
    // pieces matched the files checksum. Returns an `Err` in case of
    // error.
    fn adjust_piece(
        &mut self,
        current: &mut Current<'a>
    ) -> Result<bool, Error> {
        if current.piece_remaining != 0 {
            return Ok(false);
        }
        if self.hasher.digest().bytes() != current.piece {
            return Err(Error::ChecksumFailed);
        }
        match self.pieces.next() {
            Some(p) => {
                current.piece = p;
                current.piece_remaining = self.piece_length;
                self.hasher.reset();
                Ok(false)
            }
            None => {
                // XXX This stuffs invalid values in
                // current.file_remaining and current.files; but as
                // current.piece_remaining is 0 it will stabely comme
                // back here. I.e. this consistently terminates the
                // check.
                for res in self.files.by_ref() {
                    let (file, length) = res?;
                    current.file = file;
                    current.file_remaining += length;
                }
                if current.file_remaining > 0 {
                    return Err(Error::UnexpectedEndOfPieces);
                }
                Ok(true)
            }
        }
    }

    // Returns `Ok(true)` if there is more work to do. Returns
    // `Ok(false)` if there is nothing more to do, that is all the
    // pieces matched the files checksum. Returns an `Err` in case of
    // error.
    fn adjust_file(
        &mut self,
        current: &mut Current<'a>
    ) -> Result<bool, Error> {
        while current.file_remaining == 0 {
            match self.files.next().transpose()? {
                Some((file, length)) => {
                    current.file = file;
                    current.file_remaining = length;
                }
                None if self.pieces.next().is_some() =>
                    return Err(Error::UnexpectedEndOfFiles),
                None if self.hasher.digest().bytes() != current.piece =>
                    return Err(Error::ChecksumFailed),
                None => return Ok(true)
            }
        }
        Ok(false)
    }
}

struct Verifier<'a> {
    info: Info<'a>,
    outcome: Outcome<'a>
}

impl<'a> Verifier<'a> {
    fn new(meta_info: &'a Metainfo, base: &'a Path) -> Self {
        let base: PathBuf = std::iter::once(base)
            .chain(meta_info.info().directory().into_iter())
            .collect();
        let mut info = Info {
            piece_length: meta_info.info().piece_length(),
            pieces: meta_info.info().pieces(),
            files: FilesChecked::new(base, meta_info.info().files()),
            hasher: Sha1::new()
        };
        let outcome = Outcome::new(
            &mut info.pieces,
            &mut info.files,
            info.piece_length,
        );
        Self { info, outcome }
    }

    // Make one step: read in either a buffer, a piece or a file
    fn step(&mut self, buffer: &mut [u8]) -> Result<bool, Error> {
        match &mut self.outcome {
            Outcome::Ok => return Ok(true),
            Outcome::Err(err) => return Err(err.clone()),
            Outcome::InProgress(current) => {
                let res = current.read_one_chunk(buffer, &mut self.info);
                if let Err(err) = res {
                    self.outcome = Outcome::Err(err.clone());
                    return Err(err);
                }
                match self.info.adjust_piece(current) {
                    Err(err) => {
                        self.outcome = Outcome::Err(err.clone());
                        return Err(err);
                    }
                    Ok(true) => {
                        self.outcome = Outcome::Ok;
                        return Ok(true);
                    }
                    Ok(false) => ()
                }
                assert!(current.piece_remaining > 0);
                match self.info.adjust_file(current) {
                    Err(err) => {
                        self.outcome = Outcome::Err(err.clone());
                        return Err(err);
                    }
                    Ok(true) => {
                        self.outcome = Outcome::Ok;
                        return Ok(true);
                    }
                    Ok(false) => ()
                }
                Ok(false)
            }
        }
    }
}

fn torrent_check<P>(meta_info: &Metainfo, base: P) -> Result<(), Error>
where P: AsRef<Path>
{
    let mut verifier = Verifier::new(meta_info, base.as_ref());
    let mut buffer = [0; BUFFER_LEN];
    while ! verifier.step(&mut buffer)? {
        // No op
    }
    Ok(())
}

fn torrent_check_file<T, U>(torrent: T, base: U) -> Result<(), Error>
where T: AsRef<Path>,
      U: AsRef<Path>
{
    let bytes = std::fs::read(torrent)?;
    let meta_info = Metainfo::from_bytes(bytes)?;
    torrent_check(&meta_info, base)
}

fn main() -> Result<(), Error> {
    let args: Vec<String> = std::env::args().skip(1).collect();
    match args.as_slice() {
        [torrent] => torrent_check_file(torrent, ""),
        [torrent, base] => torrent_check_file(torrent, base),
        _ => {
            eprintln!("Usage: torrentsum <torrent-file> [base-directory]");
            std::process::exit(1);
        }
    }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;
    use super::*;

    fn path<'a, I>(s: I) -> PathBuf
    where I: IntoIterator<Item=&'a str>
    {
        let mut path = PathBuf::from(
            std::env::var("CARGO_MANIFEST_DIR").unwrap()
        );
        path.push("test-data");
        path.extend(s.into_iter());
        path
    }

    #[test]
    fn test_short() {
        assert_eq!(
            torrent_check_file(
                path(["short.txt.torrent"]),
                path(["short"])
            ),
            Ok(())
        );
    }

    #[test]
    fn test_short_checksum_failed() {
        assert_eq!(
            torrent_check_file(
                path(["short.txt.checksum-failed.torrent"]),
                path(["short"])
            ),
            Err(Error::ChecksumFailed)
        );
    }

    #[test]
    fn test_overlong() {
        assert_eq!(
            torrent_check_file(
                path(["overlong-file.txt.torrent"]),
                path(["overlong-file"])
            ),
            Err(Error::FileTooLong)
        );
    }

    #[test]
    fn test_multi_files() {
        assert_eq!(
            torrent_check_file(
                path(["multi-files.torrent"]),
                path([])
            ),
            Ok(())
        );
    }

    #[test]
    fn test_multi_files_checksum_failed() {
        assert_eq!(
            torrent_check_file(
                path(["multi-files.checksum-failed.torrent"]),
                path([])
            ),
            Err(Error::ChecksumFailed)
        );
    }

    #[test]
    fn test_multi_files_missing_one() {
        assert_eq!(
            torrent_check_file(
                path(["multi-files-missing-one.torrent"]),
                path([])
            ),
            Err(Error::Io(std::io::ErrorKind::NotFound))
        );
    }

    #[test]
    fn test_piece_length_zero() {
        assert_eq!(
            torrent_check_file(
                path(["piece-length-zero.torrent"]),
                path(["piece-length-zero"])
            ),
            Ok(())
        );
    }
}
